pragma solidity ^0.6.2;

contract Whitelist {

    address private administrateur;
    uint startingDate;
    Personne[] public whitelist;

    struct Personne {
        string nom;
        string prenom;
    }

    constructor() public{
        administrateur = msg.sender;
        startingDate = now + 10 days;
    }

    function ajouterPersonne(string memory _nom, string memory _prenom) public {
        require(now < startingDate);
        require(msg.sender == administrateur);
        whitelist.push(Personne(_nom, _prenom));
    }
}

